﻿using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.DataAccess.Abstract
{
    public interface IItemRepository : IDisposable
    {
        List<Item> GetAllList();
        long Save(Item item);
        Item GetById(int id);
        Item GetByName(string name);
        void Delete(int id);
    }
}
