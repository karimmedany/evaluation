﻿using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EV.DataAccess.Abstract
{
    public interface IUserRepository : IDisposable
    {
        List<User> GetAllList();
        long Save(User item);
        User GetById(int id);
        User GetBy(string userName,string password);
        void Delete(int id);
        IQueryable<User> GetActiveList(int page, int count, string sortExpression, out int resultsFound);
    }
}
