﻿using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.DataAccess.Abstract
{
    public interface IConversitionRepository : IDisposable
    {
        List<Conversition> GetAllList();
        long Save(Conversition item);
        Conversition GetById(int id);
        Conversition GetByName(string name);
        void Delete(int id);
    }
}
