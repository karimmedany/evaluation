﻿using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.DataAccess.Abstract
{
    public interface IBotConversationRepository : IDisposable
    {
        List<BotConversation> GetAllList();
        long Save(BotConversation item);
        BotConversation GetById(int id);

        void Delete(int id);
    }
}
