﻿using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EV.DataAccess.Abstract
{
    public interface IItemReservationRepository : IDisposable
    {
        List<ItemReservation> GetAllList();
        int Save(ItemReservation item);
        ItemReservation GetById(int id);

        ItemReservation GetBy(DateTime reservationDate);
        void Delete(int id);
        IQueryable<ItemReservation> GetActiveList(int page, int count, string sortExpression, out int resultsFound);
    }
}
