﻿using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.DataAccess.Abstract
{
    public interface IConversationParticipateRepository : IDisposable
    {
        List<ConversationParticipate> GetAllList();
        long Save(ConversationParticipate item);
        ConversationParticipate GetById(int id);
        void Delete(int id);
    }
}
