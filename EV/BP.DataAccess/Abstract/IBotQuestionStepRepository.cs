﻿using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.DataAccess.Abstract
{
    public interface IBotQuestionStepRepository : IDisposable
    {
        List<BotQuestionStep> GetAllList();
        long Save(BotQuestionStep item);
        BotQuestionStep GetById(int id);
        void Delete(int id);
    }
}
