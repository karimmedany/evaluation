﻿using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EV.DataAccess.Concrete
{
    public class ItemRepository : IItemRepository
    {
        Context context;
        public ItemRepository(Context context)
        {
            this.context = context;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Item> GetActiveList(int page, int count, string sortExpression, out int resultsFound)
        {
            resultsFound = context.Item.Count();
            int skip = Common.Global.ManageGlobal.GetSkipItemsForPaging(page, count);
            if (String.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "CreatedDate";
            }
            var result = context.Item.OrderBy(o => sortExpression).Skip(skip).Take(count);
            return result;
        }

        public List<Item> GetAllList()
        {
            throw new NotImplementedException();
        }

        public Item GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Item GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public long Save(Item item)
        {
            throw new NotImplementedException();
        }


        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                    context.Dispose();
            }
        }

        ~ItemRepository()
        {
            Dispose(false);
        }
        #endregion
    }
}
