﻿using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EV.DataAccess.Concrete
{
    public class UserRepository : IUserRepository
    {
        Context context;
        public UserRepository(Context context)
        {
            this.context = context;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }



        public IQueryable<User> GetActiveList(int page, int count, string sortExpression, out int resultsFound)
        {
            resultsFound = context.User.Count();
            int skip = Common.Global.ManageGlobal.GetSkipItemsForPaging(page, count);
            if (String.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "CreatedDate";
            }
            var result = context.User.OrderBy(o => sortExpression).Skip(skip).Take(count);
            return result;
        }

        public List<User> GetAllList()
        {
            throw new NotImplementedException();
        }

        public User GetById(int id)
        {
           return context.User.Where(i => i.Id == id).FirstOrDefault();
        }
        public User GetBy(string userName, string password)
        {
            return context.User.Where(i => i.UserName == userName && i.Password == password).FirstOrDefault();
        }

        public long Save(User item)
        {
            throw new NotImplementedException();
        }

        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                    context.Dispose();
            }
        }



        ~UserRepository()
        {
            Dispose(false);
        }
        #endregion
    }
}
