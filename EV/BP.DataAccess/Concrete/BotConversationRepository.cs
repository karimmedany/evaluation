﻿using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.DataAccess.Concrete
{
    public class BotConversationRepository : IBotConversationRepository
    {
        Context context;
        public BotConversationRepository(Context context)
        {
            this.context = context;
        }
        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<BotConversation> GetAllList()
        {
            throw new NotImplementedException();
        }

        public BotConversation GetById(int id)
        {
            throw new NotImplementedException();
        }

        public long Save(BotConversation item)
        {
            throw new NotImplementedException();
        }


        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                    context.Dispose();
            }
        }

        ~BotConversationRepository()
        {
            Dispose(false);
        }
        #endregion
    }
}
