﻿using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EV.DataAccess.Concrete
{
    public class ItemReservationRepository : IItemReservationRepository
    {
        Context context;
        public ItemReservationRepository(Context context)
        {
            this.context = context;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ItemReservation> GetActiveList(int page, int count, string sortExpression, out int resultsFound)
        {
            resultsFound = context.ItemReservation.Count();
            int skip = Common.Global.ManageGlobal.GetSkipItemsForPaging(page, count);
            if (String.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "CreatedDate";
            }
            var result = context.ItemReservation.OrderBy(o => sortExpression).Skip(skip).Take(count);
            return result;
        }

        public List<ItemReservation> GetAllList()
        {
            throw new NotImplementedException();
        }


        public ItemReservation GetBy(DateTime reservationDate)
        {
           return context.ItemReservation.Where(i => i.ReservationDate == reservationDate).FirstOrDefault();
        }

        public ItemReservation GetById(int id)
        {
            throw new NotImplementedException();
        }

        public int Save(ItemReservation item)
        {
            if (item.Id == 0)
            {
                item.CreatedDateTime = DateTime.Now;
                context.ItemReservation.Add(item);
            }
            context.SaveChanges();
            return item.Id;
        }


        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                    context.Dispose();
            }
        }

        ~ItemReservationRepository()
        {
            Dispose(false);
        }
        #endregion
    }
}
