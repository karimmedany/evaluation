﻿using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.DataAccess.Concrete
{
    public class BotQuestionStepRepository : IBotQuestionStepRepository
    {
        Context context;
        public BotQuestionStepRepository(Context context)
        {
            this.context = context;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<BotQuestionStep> GetAllList()
        {
           return context.BotQuestionStep.OrderBy(i => i.ArrangeIndex).ToList();
        }

        public BotQuestionStep GetById(int id)
        {
            throw new NotImplementedException();
        }

        public long Save(BotQuestionStep item)
        {
            throw new NotImplementedException();
        }


        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                    context.Dispose();
            }
        }

        ~BotQuestionStepRepository()
        {
            Dispose(false);
        }
        #endregion
    }
}
