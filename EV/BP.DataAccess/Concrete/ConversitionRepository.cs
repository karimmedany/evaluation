﻿using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EV.DataAccess.Concrete
{
    public class ConversitionRepository : IConversitionRepository
    {
        Context context;
        public ConversitionRepository(Context context)
        {
            this.context = context;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IQueryable<Conversition> GetActiveList(int page, int count, string sortExpression, out int resultsFound)
        {
            resultsFound = context.Conversition.Count();
            int skip = Common.Global.ManageGlobal.GetSkipItemsForPaging(page, count);
            if (String.IsNullOrEmpty(sortExpression))
            {
                sortExpression = "CreatedDate";
            }
            var result = context.Conversition.OrderBy(o => sortExpression).Skip(skip).Take(count);
            return result;
        }

        public List<Conversition> GetAllList()
        {
            throw new NotImplementedException();
        }

        public Conversition GetById(int id)
        {
            throw new NotImplementedException();
        }

        public Conversition GetByName(string name)
        {
            throw new NotImplementedException();
        }

        public long Save(Conversition item)
        {
            throw new NotImplementedException();
        }


        #region Dispose
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (context != null)
                    context.Dispose();
            }
        }

        ~ConversitionRepository()
        {
            Dispose(false);
        }
        #endregion
    }
}
