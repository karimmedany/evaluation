namespace EV.DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BotConversation")]
    public partial class BotConversation
    {
        public int Id { get; set; }

        public int UserSenderId { get; set; }

        public int BotQuestionStepId { get; set; }

        [Required]
        [StringLength(300)]
        public string Answer { get; set; }

        public virtual BotQuestionStep BotQuestionStep { get; set; }

        public virtual User User { get; set; }
    }
}
