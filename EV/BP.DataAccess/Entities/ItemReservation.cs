namespace EV.DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ItemReservation")]
    public partial class ItemReservation
    {
        public int Id { get; set; }

        public int ItemId { get; set; }

        [Column(TypeName = "date")]
        public DateTime ReservationDate { get; set; }

        public DateTime CreatedDateTime { get; set; }
    }
}
