namespace EV.DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Conversition")]
    public partial class Conversition
    {
        public int Id { get; set; }

        public int UserSenderId { get; set; }

        [Required]
        public string TextValue { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public int ConversationParticipateId { get; set; }

        public virtual ConversationParticipate ConversationParticipate { get; set; }

        public virtual User User { get; set; }
    }
}
