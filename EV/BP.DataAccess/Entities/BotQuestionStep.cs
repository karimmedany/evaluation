namespace EV.DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BotQuestionStep")]
    public partial class BotQuestionStep
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BotQuestionStep()
        {
            BotConversation = new HashSet<BotConversation>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int ArrangeIndex { get; set; }

        public string TextValue { get; set; }
        public bool? IsAnswerDate { get; set; }

        public bool? IsAnswerEmail { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BotConversation> BotConversation { get; set; }
    }
}
