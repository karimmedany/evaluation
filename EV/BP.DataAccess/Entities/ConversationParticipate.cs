namespace EV.DataAccess.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ConversationParticipate")]
    public partial class ConversationParticipate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ConversationParticipate()
        {
            Conversition = new HashSet<Conversition>();
        }

        public int Id { get; set; }

        public int UserId { get; set; }

        public bool IsOwner { get; set; }

        public DateTime CreatedDateTime { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Conversition> Conversition { get; set; }
    }
}
