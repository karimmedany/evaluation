namespace EV.DataAccess.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Context : DbContext
    {
        public Context()
            : base("name=Context")
        {
        }

        public virtual DbSet<BotConversation> BotConversation { get; set; }
        public virtual DbSet<BotQuestionStep> BotQuestionStep { get; set; }
        public virtual DbSet<ConversationParticipate> ConversationParticipate { get; set; }
        public virtual DbSet<Conversition> Conversition { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<ItemReservation> ItemReservation { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BotQuestionStep>()
                .HasMany(e => e.BotConversation)
                .WithRequired(e => e.BotQuestionStep)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversationParticipate>()
                .HasMany(e => e.Conversition)
                .WithRequired(e => e.ConversationParticipate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Item>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Status)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BotConversation)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserSenderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ConversationParticipate)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Conversition)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserSenderId)
                .WillCascadeOnDelete(false);
        }
    }
}
