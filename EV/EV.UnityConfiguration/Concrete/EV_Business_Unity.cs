﻿using EV.UnityConfiguration.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using EV.Business.Abstract;
using EV.Business.Concrete;

namespace EV.UnityConfiguration.Concrete
{
    public class EV_Business_Unity : IConfigureUnity
    {
        public void Configure(UnityContainer container)
        {
            container.RegisterType<IBotConversationManager, BotConversationManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IBotQuestionStepManager, BotQuestionStepManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IConversationParticipateManager, ConversationParticipateManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IConversitionManager, ConversationManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IItemManager, ItemManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IItemReservationManager, ItemReservationManager>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserManager, UserManager>(new HierarchicalLifetimeManager());
        }
    }
}
