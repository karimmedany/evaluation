﻿using EV.UnityConfiguration.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using EV.DataAccess.Abstract;
using EV.DataAccess.Concrete;

namespace EV.UnityConfiguration.Concrete
{
    public class EV_DataAccess_Unity : IConfigureUnity
    {
        public void Configure(UnityContainer container)
        {
            container.RegisterType<IBotConversationRepository, BotConversationRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IBotQuestionStepRepository, BotQuestionStepRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IConversationParticipateRepository, ConversationParticipateRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IConversitionRepository, ConversitionRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IItemRepository, ItemRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IItemReservationRepository, ItemReservationRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserRepository,UserRepository>(new HierarchicalLifetimeManager());

        }
    }
}
