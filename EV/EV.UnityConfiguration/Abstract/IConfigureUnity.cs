﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.UnityConfiguration.Abstract
{
    public interface IConfigureUnity
    {
        void Configure(UnityContainer container);
    }
}
