﻿
using EV.Business.Abstract;
using EV.Business.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EV.ServiceApi.Controllers
{
    public class BotQuestionStepController : ApiController
    {
        IBotQuestionStepManager _manager;
        public BotQuestionStepController(IBotQuestionStepManager manager)
        {
            _manager = manager;
        }

        public QuestionModel ManageConversation(QuestionModel inputData)
        {
            QuestionModel result = null;
            try
            {
                // check first if the user is authorized
                if (Common.Sessions.ManageSessions.UserId != null)
                    result = _manager.ManageConversation(inputData);
            }
            finally
            {
                _manager.Dispose();
            }
            return result;
        }
    }
}
