﻿using EV.Business.Abstract;
using EV.Business.Model;
using EV.ServiceApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EV.ServiceApi.Controllers
{
    public class UserController : ApiController
    {
        IUserManager _manager;

        public UserController(IUserManager manager)
        {
            _manager = manager;
        }

        public List<UserModel> GetUserByLoginUser() {
            List<UserModel> result = new List<UserModel>();
            try
            {
                // check first if user is authorized
                if (Common.Sessions.ManageSessions.UserId != null)
                {
                    result = _manager.GetAllUserByUserLogedInId(Common.Sessions.ManageSessions.UserId.Value);
                }
            }
            finally
            {
                _manager.Dispose();
            }
            return result;
        }
        [HttpPost]
        public string UserLogin(UserLogin inputData)
        {
            string result = string.Empty;
            try
            {
                UserLoginModel selected = _manager.GetLoginUser(inputData.UserName, inputData.Password);
                if (selected != null)
                {
                    Common.Sessions.ManageSessions.UserId = selected.Id;
                    result = selected.FullName;
                }
            }
            finally
            {
                _manager.Dispose();
            }
            return result;
        }

    }
}
