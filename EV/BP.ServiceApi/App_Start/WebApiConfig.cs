﻿using EV.ServiceApi.Resolver;
using EV.UnityConfiguration.Concrete;
using Microsoft.Practices.Unity;
using System.Net.Http.Headers;
using System.Web.Http;

namespace EV.ServiceApi.App_Start
{
    public class WebApiConfig
    {
        public static void Configure(HttpConfiguration config)
        {

            var container = new UnityContainer();
            new EV_DataAccess_Unity().Configure(container);
            new EV_Business_Unity().Configure(container);
            config.DependencyResolver = new UnityResolver(container);

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "CustomWithActionApi",
                routeTemplate: "{controller}/{action}/{value}",
                defaults: new { value = RouteParameter.Optional }
);
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );



            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}