﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.Business.Model
{
    public class QuestionModel
    {
        public string BotImageURL { get; set; }
        public int ArrangeIndex { get; set; }

        public string TextValue { get; set; }
        public bool? IsAnswerDate { get; set; }

        public bool? IsAnswerEmail { get; set; }
        public string UserAnswer { get; set; }
    }
}
