﻿using EV.Business.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.Business.Concrete
{
    public class ConversationParticipateManager: IConversationParticipateManager
    {
        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {



            }
        }

        ~ConversationParticipateManager()
        {
            Dispose(false);
        }

        #endregion
    }
}
