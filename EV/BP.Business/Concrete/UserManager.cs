﻿using EV.Business.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EV.Business.Model;
using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;

namespace EV.Business.Concrete
{
    public class UserManager : IUserManager
    {
        #region Private Memebers
        IUserRepository _repUser;
        #endregion
        public UserManager(IUserRepository repUser)
        {
            _repUser = repUser;
        }

        #region Public Actions

        public List<UserModel> GetAllUserByUserLogedInId(int id)
        {
            List<UserModel> result = new List<UserModel>();
           User selectUser =  _repUser.GetById(id);
            if (selectUser != null)
            {
               IEnumerable<User> selectedUsers = selectUser.ConversationParticipate.Select(i => i.User);
                result = (from item in selectedUsers where item.Id != id select new UserModel() { Id = item.Id, UserName = item.UserName, Status = item.Status }).ToList();
            }
            return result;
        }

        public UserLoginModel GetLoginUser(string userName, string password)
        {
            UserLoginModel result = null;
          User selected =  _repUser.GetBy(userName, password);
            if (selected != null)
            {
                result = new UserLoginModel();
                result.FullName = selected.FullName;
                result.Id = selected.Id; 
            }
            return result;
        }
        #endregion


        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_repUser != null)
                {
                    _repUser.Dispose();
                    _repUser = null;
                }

            }
        }

        ~UserManager()
        {
            Dispose(false);
        }

        #endregion
    }
}
