﻿using EV.Business.Abstract;
using EV.Business.Model;
using EV.DataAccess.Abstract;
using EV.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.Business.Concrete
{
    public class BotQuestionStepManager: IBotQuestionStepManager
    {
        #region Private Members

        IBotQuestionStepRepository _repBotQuestionStep;
        IItemReservationRepository _repItemReservation;
        #endregion

        public BotQuestionStepManager(IBotQuestionStepRepository repBotQuestionStep,IItemReservationRepository repItemReservation)
        {
            _repBotQuestionStep = repBotQuestionStep;
            _repItemReservation = repItemReservation;
        }

        #region Public Actions
        public QuestionModel ManageConversation(QuestionModel input)
        {
            QuestionModel result = new QuestionModel();
            if (input == null)
            {
                input = new QuestionModel();
                input.ArrangeIndex = 0;
            }
            List<BotQuestionStep> selectedList = _repBotQuestionStep.GetAllList();
            if (!string.IsNullOrEmpty(input.UserAnswer))
            {
               
                BotQuestionStep selected = selectedList.Where(i => i.ArrangeIndex == input.ArrangeIndex + 1).FirstOrDefault();
                if (selected == null)
                {
                    if (input.IsAnswerDate != null && input.IsAnswerDate.Value)
                    {
                        DateTime reservationDate = Convert.ToDateTime(input.UserAnswer);
                        ItemReservation selectedReservation = _repItemReservation.GetBy(reservationDate);
                        if (selectedReservation == null)
                        {
                            ItemReservation item = new ItemReservation();
                            item.ItemId = 1;
                            item.ReservationDate = reservationDate;
                            int selectedId = _repItemReservation.Save(item);
                            if (selectedId > 0)
                            {
                                // we can use the resurces files to manage the languages and avoid write hard code
                                result.TextValue = "The item has been booked, will recive a confirmation email soon.";
                            }
                            else
                            {
                                result.TextValue = "The item is available, but something wrong happend. Please call 00000";
                            }

                        }
                        else
                        {
                           
                            result.TextValue = "The item is not available, please try again";
                        }

                        //TO DO:  save the conversation if you need a reference in "BotConversation"
                    }
                }
                else
                {

                    result.BotImageURL = "../../Content/images/dummy50.jpg";
                    result.ArrangeIndex = selected.ArrangeIndex;
                    result.IsAnswerDate = selected.IsAnswerDate;
                    result.IsAnswerEmail = selected.IsAnswerEmail;
                    result.TextValue = selected.TextValue;


                }


            }
            else
            {
                BotQuestionStep selected = selectedList.Where(i => i.ArrangeIndex == 1).FirstOrDefault();
                if (selected != null)
                {
                    result.BotImageURL = "../../Content/images/dummy50.jpg";
                    result.ArrangeIndex = selected.ArrangeIndex;
                    result.IsAnswerDate = selected.IsAnswerDate;
                    result.IsAnswerEmail = selected.IsAnswerEmail;
                    result.TextValue = selected.TextValue;
                }

            }
            return result;
        }
        #endregion

        #region Dispose

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_repBotQuestionStep != null)
                {
                    _repBotQuestionStep.Dispose();
                    _repBotQuestionStep = null;
                }
                if (_repItemReservation != null)
                {
                    _repItemReservation.Dispose();
                    _repItemReservation = null;
                }

            }
        }

        ~BotQuestionStepManager()
        {
            Dispose(false);
        }

        #endregion
    }
}
