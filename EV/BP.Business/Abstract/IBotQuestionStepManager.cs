﻿
using EV.Business.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.Business.Abstract
{
    public interface IBotQuestionStepManager : IDisposable
    {
        /// <summary>
        /// For managing the conversation with Bot
        /// </summary>
        /// <param name="input">QestionModel: custom model</param>
        /// <returns>return: different model after modifications</returns>
        QuestionModel ManageConversation(QuestionModel input);
    }
}
