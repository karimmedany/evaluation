﻿
using EV.Business.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace EV.Business.Abstract
{
    public interface IUserManager : IDisposable
    {
        /// <summary>
        /// return all users related to certian user authorized
        /// </summary>
        /// <param name="UserId">login user id</param>
        /// <returns></returns>
        List<UserModel> GetAllUserByUserLogedInId(int id);

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        UserLoginModel GetLoginUser(string userName, string password);
    }
}
