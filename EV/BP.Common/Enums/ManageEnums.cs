﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EV.Common.Enums
{
    /// <summary>
    /// File extention allowed
    /// </summary>
    public enum AllowExtention
    {
        pdf, jpg, jpeg, png
    }

    public enum Lang
    {
        En, Ar
    }
    /// <summary>
    /// User Status flag to know the status of each user
    /// </summary>
    public enum UserStatus
    {
        // as the description of the database field
        Online,Busy,Offline
    }
}
