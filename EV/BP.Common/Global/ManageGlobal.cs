﻿using EV.Common.Enums;
using EV.Common.Sessions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace EV.Common.Global
{
    public static class ManageGlobal
    {
        private const string AesIV256 = @"!QAZ2WSX#EDC4RFV";
        private const string AesKey256 = @"5TGB&YHN7UJM(IK<5TGB&YHN7UJM(IK<";


        public static string Encrypt(string value)
        {
            //return Encrypt(value, string.Empty);
            return encrypt256(value);
        }

        static private string encrypt256(string text)
        {
            string result = string.Empty;
            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 256;
            aes.IV = Encoding.UTF8.GetBytes(AesIV256);
            aes.Key = Encoding.UTF8.GetBytes(AesKey256);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // Convert string to byte array
            byte[] src = Encoding.Unicode.GetBytes(text);

            // encryption
            using (ICryptoTransform encrypt = aes.CreateEncryptor())
            {
                byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

                // Convert byte array to Base64 strings
                string Base64 = Convert.ToBase64String(dest);
                result = HttpUtility.UrlEncode(Base64);
            }
            return result;
        }


        public static string Decrypt(string value)
        {
            string result = null;
            if (!string.IsNullOrEmpty(value))
            {
                if (value.Equals("null") == false)
                {
                    if (value.Equals("undefined") == false)
                    {
                        //result = Decrypt(value, string.Empty);
                        result = decrypt256(value);
                    }
                }
            }
            return result;
        }

        static private string decrypt256(string text)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(text))
            {
                text = HttpUtility.UrlDecode(text);
                // AesCryptoServiceProvider
                AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
                aes.BlockSize = 128;
                aes.KeySize = 256;
                aes.IV = Encoding.UTF8.GetBytes(AesIV256);
                aes.Key = Encoding.UTF8.GetBytes(AesKey256);
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;

                // Convert Base64 strings to byte array
                byte[] src = System.Convert.FromBase64String(text);

                // decryption
                using (ICryptoTransform decrypt = aes.CreateDecryptor())
                {
                    byte[] dest = decrypt.TransformFinalBlock(src, 0, src.Length);
                    result = Encoding.Unicode.GetString(dest);
                }
            }
            return result;
        }







        public static int GetSkipItemsForPaging(int page, int count)
        {
            if (page < 2)
            {
                return 0;
            }
            else
            {
                return page * count;

            }
        }
    }
}
