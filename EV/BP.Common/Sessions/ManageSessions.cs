﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace EV.Common.Sessions
{
    public static class ManageSessions
    {
        private const string _userIdKey = "UserId";

        public static int? UserId
        {
            get
            {
                if (HttpContext.Current.Session[_userIdKey] == null)
                {

                    HttpContext.Current.Session.Add(_userIdKey, null);
                    return (int?)HttpContext.Current.Session[_userIdKey];
                }
                else
                {
                    return (int?)HttpContext.Current.Session[_userIdKey];
                }
            }
            set
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session[_userIdKey] == null)
                {
                    HttpContext.Current.Session.Add(_userIdKey, null);
                }
                HttpContext.Current.Session[_userIdKey] = value;

            }

        }


        private const string _langKey = "Lang";

        public static Enums.Lang Lang
        {
            get
            {
                if (HttpContext.Current.Session[_langKey] == null)
                {

                    HttpContext.Current.Session.Add(_langKey, Enums.Lang.Ar);
                    return (Enums.Lang)HttpContext.Current.Session[_langKey];
                }
                else
                {
                    return (Enums.Lang)HttpContext.Current.Session[_langKey];
                }
            }
            set
            {
                if (HttpContext.Current.Session == null || HttpContext.Current.Session[_langKey] == null)
                {
                    HttpContext.Current.Session.Add(_langKey, Enums.Lang.Ar);
                }
                HttpContext.Current.Session[_langKey] = value;

            }

        }





    }
}
