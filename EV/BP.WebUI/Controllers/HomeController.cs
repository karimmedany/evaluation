﻿using EV.WebUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EV.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public JsonResult GetList()
        {
            List<MenuItem> result = new List<Models.MenuItem>();
            result.Add(new MenuItem() { Id = 0, Name = "Karim", URL = "test 1" });
            result.Add(new MenuItem() { Id = 1, Name = "Medany", URL = "test 2" });
            result.Add(new MenuItem() { Id = 2, Name = "Ibrahim", URL = "test 3" });
            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }


        public JsonResult GetCountryList()
        {
            List<MenuItem> result = new List<Models.MenuItem>();
            result.Add(new MenuItem() { Id = 0, Name = "Egypt", URL = "test 1" });
            result.Add(new MenuItem() { Id = 1, Name = "UK", URL = "test 2" });
            result.Add(new MenuItem() { Id = 2, Name = "US", URL = "test 3" });
            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetCityList(int countryId)
        {
            List<MenuItem> result = new List<Models.MenuItem>();
            if (countryId == 0)
            {
                result.Add(new MenuItem() { Id = 0, Name = "Alexandria", URL = "test 1" });
                result.Add(new MenuItem() { Id = 1, Name = "Cairo", URL = "test 2" });
                result.Add(new MenuItem() { Id = 2, Name = "Luxor", URL = "test 3" });
            }
            if (countryId == 1)
            {
                result.Add(new MenuItem() { Id = 0, Name = "London", URL = "test 1" });
                result.Add(new MenuItem() { Id = 1, Name = "Liverpool", URL = "test 2" });
                result.Add(new MenuItem() { Id = 2, Name = "Lister City", URL = "test 3" });
            }
            if (countryId == 2)
            {
                result.Add(new MenuItem() { Id = 0, Name = "New York", URL = "test 1" });
                result.Add(new MenuItem() { Id = 1, Name = "Florida", URL = "test 2" });
                result.Add(new MenuItem() { Id = 2, Name = "Chicago", URL = "test 3" });
            }
            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult UserLogin(UserData inputData)
        {
            UserData result = null;
            if (inputData.UserName == "test" && inputData.Password == "test")
            {

                result = new UserData();
                result.UserName = "Hello";
                result.Password = "World";
            }
            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}