﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EV.WebUI.Models
{
    public class UserData
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}