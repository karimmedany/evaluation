﻿
angular.module("BPApp").controller("Index", function ($scope ,$window, serviceActionLogin) {
    $scope.Message = "Hello world.!";
    $scope.IsLogedIn = false;
    $scope.IsSubmited = false;
    $scope.IsFormValid = false;

    $scope.IsChattng = false;


    $scope.UserData = { UserName: '', Password: '' };
    $scope.$watch('LoginForm.$valid', function (newValue) {
        $scope.IsFormValid = newValue;
    });

    $scope.On_Login = function () {
        debugger;
        $scope.IsSubmited = true;
        if ($scope.IsFormValid) {
            serviceActionLogin.UserLogin($scope.UserData).then(function (result) {
                debugger;
                $scope.IsLogedIn = true;
                $scope.IsChattng = true;
                if (result.data !== null) {
                    
                    $scope.Message = 'Login successful, wellcome ' + result.data;
                }
                else {

                    $scope.Message = 'Login faild.!';
                }

            });
        }

    }

    $scope.On_GoChattingRoom = function () {

        $window.open('/chatting/room', '_blank');

    }
}
)
.factory('serviceActionLogin', function ($http) {
    var fac = {};
    fac.UserLogin = function (inputData) {
        return $http({
            url: '/service/user/UserLogin',
            data: JSON.stringify(inputData),
            method: 'POST',
            headers: { 'content-type': 'application/json' }
        });
    }
    return fac;
});