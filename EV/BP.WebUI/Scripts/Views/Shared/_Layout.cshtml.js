﻿
var moduleBPApp = angular.module("BPApp", ['ngRoute']); //

moduleBPApp.controller("Layout", function ($scope) {
    $scope.Message = 'Hello world lets see angular working';
});

moduleBPApp.config(function ($routeProvider) {
    $routeProvider.
    when('/Login', {
        templateUrl: 'User/Login'
        ,
        controller: 'Login'
    }).
      when('/ProductItem', {
         templateUrl: 'Product/ProductItem'
        ,
        controller: 'ProductItem'
     }).
    when('/About', {
        templateUrl: '/Home/About',
        controller: 'About'
    })
    .
    when('/Contact', {
        templateUrl: '/Home/Contact',
        controller: 'Contact'
    })
})
