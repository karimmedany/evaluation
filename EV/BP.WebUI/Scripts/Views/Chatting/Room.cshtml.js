﻿var INTEGER_REGEXP = /^-?\d+$/
var EMAIL_REGEXP = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
var DATE_REGEXP = /^(0?[1-9]|1[012])\/(0?[1-9]|[12][0-9]|3[01])\/((19\d{2})|([2-9]\d{3}))$/;
angular.module("BPApp").controller("Room", function ($scope, serviceActionRoom) {
    $scope.Message = "";
    $scope.IsLogedIn = false;
    $scope.IsSubmited = false;
    $scope.IsFormValid = false;

    $scope.QuestionModel = { };
    $scope.Conversation = [];

    $scope.AnswerText = "";
    $scope.IsValidEmail = true;
    $scope.IsValidMobileNumber = true;
    $scope.IsValidDate = true;

    $scope.On_SendAnswer = function () {
        debugger;

        var selectedLast = $scope.Conversation[$scope.Conversation.length - 1];
        if (selectedLast !== null) {
            if (selectedLast.IsAnswerEmail) {
                var isEmail = EMAIL_REGEXP.test($scope.AnswerText);
                $scope.IsValidEmail = isEmail;
            }
            if (selectedLast.IsAnswerDate) {
                var isDate = DATE_REGEXP.test($scope.AnswerText);
                $scope.IsValidDate = isDate;
            }
        }

        $scope.QuestionModel = selectedLast;
        $scope.QuestionModel.UserAnswer = $scope.AnswerText;
        if ($scope.IsValidEmail && $scope.IsValidDate) {
            serviceActionRoom.ManageConversation($scope.QuestionModel).then(function (result) {
                $scope.Conversation.push(result.data);
                $scope.AnswerText = "";
            });
    }
    };

    $scope.init = function () {
        serviceActionRoom.ManageConversation($scope.QuestionModel).then(function (result) {
            debugger;
            $scope.Conversation.push(result.data);
    });

    };
}
)
.factory('serviceActionRoom', function ($http) {
    var fac = {};
    fac.ManageConversation = function (inputData) {
        return $http({
            url: '/service/BotQuestionStep/ManageConversation',
            data: JSON.stringify(inputData),
            method: 'POST',
            headers: { 'content-type': 'application/json' }
        });
    }
    return fac;
});