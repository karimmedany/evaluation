﻿angular.module("BPApp", ['ng-Route'])
.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when("/", {
            redirectTo: function () { return '/home'; }
        })
            .when("/home", {
                templateUrl: 'home/index',
                controller: 'Index'
            })
                .when("/about", {
                    templateUrl: 'home/about',
                    controller: 'About'
                })
                    .when("/contact", {
                        templateUrl: 'home/contact',
                        controller: 'Contact'
                    })
    .otherwise(
    {
        templateUrl: '/Error',
        controller:'Error'
    }
    )

})